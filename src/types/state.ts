export type State = {
	invoiceNumber: string
	reference: string
	invoiceDate: string
	dueDays: string
	note: string
	iban: string
	swiftBic: string

	payerName: string
	payerAddress1: string
	payerAddress2: string
	payerCountry: string
	payerVat: string

	companyName: string
	companyAddress1: string
	companyAddress2: string
	companyCountry: string
	companyYtunnus: string
	companyVat: string
	companyPhone: string
	companyEmail: string
	companyWww: string

	bankName: string
	bankAddress1: string
	bankAddress2: string
}
