# Laskupohja

Template for creating Finnish invoices.

Fill out the form and print the invoice (as a PDF).
Store invoice details by clicking the "Update URL" button, and copy the URL or bookmark the page.

> The original codebase was lost, this is a rewrite based on a recovered cached browser html page.

## TODO

- Add international reference number format & barcode v5
- [x] Add international invoice form
- move barcode code fro root into the component where it is used
- VAT breakdown
